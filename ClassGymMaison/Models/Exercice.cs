﻿
using System.ComponentModel.DataAnnotations;


namespace ClassGymMaison.Models
{
    public class Exercice 
    {
        [Key]
        public int IdExercice { get; set; }
        public string NomExercice { get; set; }
        public string DescriptionExercice { get; set; }

    }
}
