﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClassGymMaison.Models
{
    public class ExerciceEntrainement
    {
        [Key]
        public int IdExerciceEntrainement { get; set; }
        [ForeignKey("Exercice")]
        public int IdExercice { get; set; }
        [ForeignKey("Entrainement")]
        public int IdEntrainement { get; set; }

        public int DureeExercice { get; set; }
        public int OrdreExercice { get; set; }
    }
}

