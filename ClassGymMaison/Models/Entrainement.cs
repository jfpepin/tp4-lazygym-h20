﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace ClassGymMaison.Models
{
    public class Entrainement : IComparable<Entrainement>
    {
        [Key]
        public int IdEntrainement { get; set; }
        public string NomEntrainement { get; set; }

        /*
        public int GroupeId { get; set; }
        public Groupe Groupe { get; set; }
        public ICollection<Critere> Criteres { get; set; }
        public ICollection<Equipe> Equipes { get; set; }
        public ICollection<EtudiantEvaluation> EtudiantEvaluations { get; set; }
        */

        public int CompareTo(Entrainement autreEntr)
        {
            return IdEntrainement.CompareTo(autreEntr.IdEntrainement);
        }

    }
}
