﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ClassGymMaison.Models;
using Microsoft.EntityFrameworkCore;
using Windows.Storage;

namespace ClassGymMaison.DAL
{
    public class Contexte : DbContext
    {
        private static bool _created = false;
        public Contexte()
        {
            if (!_created)
            {
                _created = true;
                Database.EnsureDeleted();
                Database.EnsureCreated();
                InitialisateurBD.Seed();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /* Ajout des modèles */
            modelBuilder.Entity<Entrainement>();
            modelBuilder.Entity<Exercice>();
            modelBuilder.Entity<ExerciceEntrainement>();

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var dbPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "GymMaison.db");
            optionsBuilder.UseSqlite("Filename = " + dbPath);
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<Entrainement> Entrainements { get; set; }
        public DbSet<Exercice> Exercices { get; set; }
        public DbSet<ExerciceEntrainement> ExercicesEntrainements { get; set; }
    }
}
