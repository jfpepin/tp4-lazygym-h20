﻿using System.Collections.Generic;
using System.Linq;
using ClassGymMaison.Models;

namespace ClassGymMaison.DAL
{
    public static class InitialisateurBD
    {
        public static void Seed()
        {
            var contexte = new Contexte();

            if (!contexte.Exercices.Any())
            {
                /* --------- */
                /* EXERCICES */
                /* --------- */

                List<Exercice> exercices = new List<Exercice>();

                exercices.Add(new Exercice()
                {
                    NomExercice = "Jogging",
                    DescriptionExercice = "Jogging extérieur dans la banlieue déserte"
                });

                exercices.Add(new Exercice()
                {
                    NomExercice = "Push-ups",
                    DescriptionExercice = "Push-ups sur banc"
                });

                exercices.Add(new Exercice()
                {
                    NomExercice = "Planche",
                    DescriptionExercice = "Planche maintenue au sol"
                });

                exercices.Add(new Exercice()
                {
                    NomExercice = "Vélo",
                    DescriptionExercice = "Vélo dans sentier du parc"
                });

                exercices.Add(new Exercice()
                {
                    NomExercice = "Position de Yoga 1",
                    DescriptionExercice = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pulvinar lacus id dignissim aliquet. Mauris vel elementum massa, a feugiat eros. "
                });

                exercices.Add(new Exercice()
                {
                    NomExercice = "Position de Yoga 2",
                    DescriptionExercice = "Fusce pulvinar lacus id dignissim aliquet. Mauris vel elementum massa, a feugiat eros. "
                });


                /* Ajout des données à la table */
                foreach (Exercice exe in exercices)
                {
                    contexte.Exercices.Add(exe);
                }

                /*Sauvegarder du contexte */
                contexte.SaveChanges();

                /* ------------ */
                /* ENTRAINEMENTS */
                /* ------------ */

                List<Entrainement> entrainements = new List<Entrainement>();

                entrainements.Add(new Entrainement() { NomEntrainement = "Entrainement cardio" });
                entrainements.Add(new Entrainement() { NomEntrainement = "Entrainement musculaire quotidien" });
                entrainements.Add(new Entrainement() { NomEntrainement = "Séance de yoga" });

                /* Ajout des données à la table */
                foreach (Entrainement ent in entrainements)
                {
                    contexte.Entrainements.Add(ent);
                }

                /*Sauvegarder du contexte */
                contexte.SaveChanges();

                /* ---------------------- */
                /* EXERCICES_ENRAINEMENTS */
                /* ---------------------- */

                List<ExerciceEntrainement> exercicesEntr = new List<ExerciceEntrainement>();

                /* Exercices pour Entrainement 0 */

                exercicesEntr.Add(new ExerciceEntrainement()
                {
                    IdExercice = exercices[0].IdExercice,
                    IdEntrainement = entrainements[0].IdEntrainement,
                    DureeExercice = 1800,
                    OrdreExercice = 1
                }
                );

                exercicesEntr.Add(new ExerciceEntrainement()
                {
                    IdExercice = exercices[3].IdExercice,
                    IdEntrainement = entrainements[0].IdEntrainement,
                    DureeExercice = 1800,
                    OrdreExercice = 2
                }
                );

                /* Exercices pour Entrainement 1 */

                exercicesEntr.Add(new ExerciceEntrainement()
                {
                    IdExercice = exercices[1].IdExercice,
                    IdEntrainement = entrainements[1].IdEntrainement,
                    DureeExercice = 30,
                    OrdreExercice = 1
                }
                );

                exercicesEntr.Add(new ExerciceEntrainement()
                {
                    IdExercice = exercices[2].IdExercice,
                    IdEntrainement = entrainements[1].IdEntrainement,
                    DureeExercice = 60,
                    OrdreExercice = 2
                }
                );

                exercicesEntr.Add(new ExerciceEntrainement()
                {
                    IdExercice = exercices[1].IdExercice,
                    IdEntrainement = entrainements[1].IdEntrainement,
                    DureeExercice = 45,
                    OrdreExercice = 3
                }
                );

                exercicesEntr.Add(new ExerciceEntrainement()
                {
                    IdExercice = exercices[2].IdExercice,
                    IdEntrainement = entrainements[1].IdEntrainement,
                    DureeExercice = 120,
                    OrdreExercice = 4
                }
                );

                /* Exercices pour Entrainement 2 */

                exercicesEntr.Add(new ExerciceEntrainement()
                {
                    IdExercice = exercices[4].IdExercice,
                    IdEntrainement = entrainements[2].IdEntrainement,
                    DureeExercice = 120,
                    OrdreExercice = 1
                }
                );

                exercicesEntr.Add(new ExerciceEntrainement()
                {
                    IdExercice = exercices[5].IdExercice,
                    IdEntrainement = entrainements[2].IdEntrainement,
                    DureeExercice = 150,
                    OrdreExercice = 2
                }
                );

                exercicesEntr.Add(new ExerciceEntrainement()
                {
                    IdExercice = exercices[4].IdExercice,
                    IdEntrainement = entrainements[2].IdEntrainement,
                    DureeExercice = 60,
                    OrdreExercice = 3
                }
                );

                /* Ajout des données à la table */
                foreach (ExerciceEntrainement exeEnt in exercicesEntr)
                {
                    contexte.ExercicesEntrainements.Add(exeEnt);
                }

                /*Sauvegarder du contexte */
                contexte.SaveChanges();

            }
        }
    }
}
