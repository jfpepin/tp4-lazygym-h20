﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using ClassGymMaison.Models;
using ClassGymMaison.DAL;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace GymMaison
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class Entrainements : Page
    {
        private readonly Contexte _contexte = new Contexte();

        public Entrainements()
        {
            InitializeComponent();
            var entrainements = _contexte.Entrainements.ToList();
            ListEntrainements.ItemsSource = entrainements;
        }
        private void AjouterEntrainement_Click(object sender, RoutedEventArgs e)
        {
            string nomEntrainement = Txt_NomEntrainement.Text;
            if (string.IsNullOrEmpty(nomEntrainement))
            {
                Msg_AjouterEntrainement.Foreground = new SolidColorBrush(Colors.Red);
                Msg_AjouterEntrainement.Text = "Veuillez entrer un nom d'entraînement";
            }
            else
            {

                var nouveauEntrainement = new Entrainement()
                {
                    NomEntrainement = nomEntrainement
                };
                _contexte.Entrainements.Add(nouveauEntrainement);
                _contexte.SaveChanges();

                Txt_NomEntrainement.Text = Msg_AjouterEntrainement.Text = "";


                FlyoutAjouterEntrainement.Hide();
                Frame.Navigate(typeof(EntrainementFiche), nouveauEntrainement.IdEntrainement);

            }
        }

        private void ListEntrainements_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var index = ListEntrainements.SelectedIndex;
            var entrainements = (ListView)sender;

            if (entrainements.Items != null && entrainements.Items.Count >= index)
            {
                Entrainement entrainement = (Entrainement)entrainements.Items[index];
                Frame.Navigate(typeof(EntrainementFiche), entrainement.IdEntrainement);
            }
        }


    }
}
