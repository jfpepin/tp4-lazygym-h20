﻿
using System;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using ClassGymMaison.DAL;
using ClassGymMaison.Models;
using System.Collections.Generic;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace GymMaison
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class EntrainementFiche : Page
    {
        private int? _Id = null;
        private readonly Contexte _contexte = new Contexte();

        public EntrainementFiche()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null) _Id = (int)e.Parameter;
            if (_Id != null)
            {
                var entrainement = _contexte.Entrainements.Where(x => x.IdEntrainement == _Id).FirstOrDefault();

                if (entrainement != null)
                {
                    NomEntraintement.Text = entrainement.NomEntrainement;

                    var exercicesEntrainement = _contexte.ExercicesEntrainements.OrderBy(x => x.OrdreExercice).Where(x => x.IdEntrainement == _Id);

                    /* Données de la liste d'exercices */
                    List<object> DataExercices = new List<object>();

                    foreach (ExerciceEntrainement exeEnt in exercicesEntrainement)
                    {
                        var exercice = _contexte.Exercices.FirstOrDefault(x => x.IdExercice == exeEnt.IdExercice);

                        DataExercices.Add(new
                        {
                            exeEnt.OrdreExercice,
                            exeEnt.IdExerciceEntrainement,
                            exeEnt.DureeExercice,
                            exercice.NomExercice,
                            exercice.DescriptionExercice,
                        });
                    }

                    ListExercices.ItemsSource = DataExercices;
                }

            }
        }

        private void AjouterExercice_Click(object sender, RoutedEventArgs e)
        {
            /* TODO : Compléter Ajouter Exercice */
        }

        private void EffacerExercice_click(object sender, RoutedEventArgs e)
        {
            var bouton = (Button)sender;
            var id = (int)bouton.Tag;
            if (_contexte.ExercicesEntrainements.Any(x => x.IdExerciceEntrainement == id))
            {
                var ExeEnt = _contexte.ExercicesEntrainements.FirstOrDefault(x => x.IdExerciceEntrainement == id);
                if (ExeEnt != null)
                {
                    _contexte.ExercicesEntrainements.Remove(ExeEnt);
                }
                _contexte.SaveChanges();
            }
            Frame.Navigate(typeof(EntrainementFiche), _Id);
        }
        
    }
}
