﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using ClassGymMaison.Models;
using ClassGymMaison.DAL;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace GymMaison
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class Exercices : Page
    {
        private readonly Contexte _contexte = new Contexte();

        public Exercices()
        {
            InitializeComponent();
            var entrainements = _contexte.Exercices.ToList();
            ListesExercices.ItemsSource = entrainements;
        }

        private void Exercices_SelectionChanged(object sender, RoutedEventArgs e)
        {
            //* TODO *//
            //* Modifications d'exercices ? *//
        }
    }
}
